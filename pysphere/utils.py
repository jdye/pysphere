import pysphere
import collections

from cclib.monkey import patches_type

import types

import functools

@patches_type(pysphere.ZSI.generate.pyclass.pyclass_type, '__new__')
def pyclass_new(f, *args, **kwargs):
    thingy = f(*args, **kwargs)
    # thingy.__repr__ = holder_repr     # repr was not meant for indentation, i guess
    thingy.dictify = objectify_dynamicholder
    return thingy


def objectify_dynamicholder(dd):
    try:
        if dd.__metaclass__ == pysphere.ZSI.generate.pyclass.pyclass_type:
            return objectify_dynamicholder(dd.__dict__)
    except Exception, e:
        pass

    if isinstance(dd, collections.Sequence) and not isinstance(dd, basestring):
        return map(objectify_dynamicholder, dd)
    elif isinstance(dd, collections.Mapping):
        d = {}
        for k, v in dd.iteritems():
            d[k] = objectify_dynamicholder(v)
        return d
    else:
        return dd

@patches_type(pysphere.vi_property.VIProperty, '__new__')
def viproperty_new(f, *args, **kwargs):
    thingy = f(*args, **kwargs)
    thingy.iterparents = types.MethodType(iter_propparents, thingy)
    # thingy.iterparents = functools.partial(iter_propparents, thingy)      # this works too but is kind of hacky
    return thingy

def iter_propparents(pp):
    if hasattr(pp, 'parent'):
        parent = pp.parent

        while True:
            yield (parent.name, parent._obj._attrs['type'],)

            if not hasattr(parent, 'parent'):
                break

            parent = parent.parent
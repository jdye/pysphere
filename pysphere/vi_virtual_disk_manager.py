from pysphere.resources import VimService_services as VI
from pysphere.vi_property import VIProperty
from pysphere.resources.vi_exception import VIException, VIApiException, \
    UnsupportedPerfIntervalError, FaultTypes
from pysphere.vi_mor import VIMor
from pysphere.vi_task import VITask
from pysphere.resources import VimService_services, VimService_services_types

class VirtualDiskManager:
    def __init__(self, server, mor):
        """
        :type server: VIServer
        :type mor: VIMor
        """
        self._server = server
        self._mor = mor
        self._properties = VIProperty(server, mor)

    def CopyVirtualDisk(self, sourceName, sourceDatacenter, destName, destDatacenter, destSpecAdapterType='busLogic', destSpecDiskType='preallocated', force=False, sync_run=True):
        """
        :type sourceName: basestring
        :type sourceDatacenter: VIMor
        :type destName: basestring
        :type destDatacenter: VIMor
        :type destSpecAdapterType: basestring
        :param destSpecAdapterType: one of busLogic, ide, lsiLogic
        :type destSpecDiskType: basestring
        :param destSpecDiskType: one of:
        eagerZeroedThick-An eager zeroed thick disk has all space allocated and wiped clean of any previous contents on the physical media at creation time. Such disks may take longer time during creation compared to other disk formats.
        flatMonolithic-A preallocated monolithic disk. Disks in this format can be used with other VMware products. This format is only applicable as a destination format in a clone operation, and not usable for disk creation.
        preallocated-A preallocated disk has all space allocated at creation time and the space is zeroed on demand as the space is used.
        raw-Raw device.
        rdm-Virtual compatibility mode raw disk mapping. An rdm virtual disk grants access to the entire raw disk and the virtual disk can participate in snapshots.
        rdmp-Physical compatibility mode (pass-through) raw disk mapping. An rdmp virtual disk passes SCSI commands directly to the hardware, but the virtual disk cannot participate in snapshots.
        seSparse-A sparse (allocate on demand) format with additional space optimizations.
        sparse2Gb-A sparse disk with 2GB maximum extent size. Disks in this format can be used with other VMware products. The 2GB extent size makes these disks easier to burn to dvd or use on filesystems that don't support large files. This format is only applicable as a destination format in a clone operation, and not usable for disk creation.
        sparseMonolithic-A sparse monolithic disk. Disks in this format can be used with other VMware products. This format is only applicable as a destination format in a clone operation, and not usable for disk creation.
        thick-A thick disk has all space allocated at creation time. This space may contain stale data on the physical media. Thick disks are primarily used for virtual machine clustering, but they are generally insecure and should not be used. Due to better performance and security properties, the use of the 'preallocated' format is preferred over this format.
        thick2Gb-A thick disk with 2GB maximum extent size. Disks in this format can be used with other VMware products. The 2GB extent size makes these disks easier to burn to dvd or use on filesystems that don't support large files. This format is only applicable as a destination format in a clone operation, and not usable for disk creation.
        thin-Space required for thin-provisioned virtual disk is allocated and zeroed on demand as the space is used.
        :type force: bool
        :type sync_run: bool
        :return: VITask
        :rtype: VITask
        :raises: VIApiException
        """
        if not VIMor.is_mor(sourceDatacenter):
            sourceDatacenter = VIMor(sourceDatacenter, 'Datacenter')

        if not VIMor.is_mor(destDatacenter):
            destDatacenter = VIMor(destDatacenter, 'Datacenter')

        request = VI.CopyVirtualDisk_TaskRequestMsg()
        _this = request.new__this(self._mor)
        request.set_element__this(_this)

        request.SourceName = sourceName
        request.DestName = destName
        request.SourceDatacenter = sourceDatacenter
        request.DestDatacenter = destDatacenter
        request.Force = force

        ds = request.new_destSpec()
        ds.AdapterType = destSpecAdapterType
        ds.DiskType = destSpecDiskType
        request.DestSpec = ds

        return self._generic_task(request, self._server._proxy.CopyVirtualDisk_Task, sync_run)

    ###################
    # Private methods #
    ###################

    def _generic_task(self, request, taskfunc, sync_run=True):
        task = taskfunc(request)._returnval
        vi_task = VITask(task, self._server)

        try:
            if sync_run:
                status = vi_task.wait_for_state([vi_task.STATE_SUCCESS, vi_task.STATE_ERROR])
                if status == vi_task.STATE_ERROR:
                    raise VIException(vi_task.get_error_message(), FaultTypes.TASK_ERROR)
            return vi_task
        except (VI.ZSI.FaultException), e:
            raise VIApiException(e)

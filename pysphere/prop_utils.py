#!/usr/bin/env python

import sys
#### uncomment for help debugging
# import IPython
# sys.excepthook = IPython.core.ultratb.FormattedTB(mode='Verbose', color_scheme='Linux', call_pdb=1)

import pysphere
from pysphere.vi_mor import VIMor

from pysphere.resources import VimService_services as VI

import time

import logging

logger = logging.getLogger(__name__)


class benchmark(object):
    def __init__(self, name, outputfunc=None):
        self.outputfunc = outputfunc
        self.name = name

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, ty, val, tb):
        end = time.time()
        msg = "%s : %0.6f seconds" % (self.name, end - self.start,)
        if self.outputfunc is None:
            print msg
        else:
            self.outputfunc(msg)
        return


class PropertiesRequest(object):
    # returns an iterator that will retrieve all of the properties you asked for
    # TODO pipeline for a little more speed
    # TODO detect session timeout and reconnect
    def RetrieveProperties(self, vc):
        do_reconnect = True

        while True:
            if vc.get_api_version() >= '4.1':
                count = 1

                with benchmark('retrievepropertiesex request %d' % count, logger.debug):
                    ret = PropertiesRequest._doProxyCall(vc._proxy.RetrievePropertiesEx, self.req)._returnval

                if ret is not None and ret:
                    print 'got %d' % len(ret.Objects)

                    yield ret.Objects

                    while hasattr(ret, 'Token'):
                        newreq = PropertiesRequest._getContinueRequest(self.req, ret.Token, vc)

                        with benchmark('retrievepropertiesex request %d' % count, logger.debug):
                            ret = PropertiesRequest._doProxyCall(vc._proxy.ContinueRetrievePropertiesEx,
                                                                 newreq)._returnval

                        # does it ever return none on a continuation call?
                        assert (ret is not None)

                        print 'got %d' % len(ret.Objects)

                        yield ret.Objects

                elif ret is None and do_reconnect:
                    logger.info('session appears to have expired, attempting reconnect')
                    vc._reconnect()
                    do_reconnect = False
                    continue

            else:
                raise Exception('unsupported api version: ' + vc.get_api_version())

            break

    # TODO catch potential proxy call exceptions
    @classmethod
    def _doProxyCall(cls, f, *args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception, e:
            e_cls, exc, tb = sys.exc_info()
            raise e_cls, exc, tb

    @classmethod
    def _getContinueRequest(cls, request, token, vc):
        request = VI.ContinueRetrievePropertiesExRequestMsg()
        propcollector = request.new__this(vc._do_service_content.PropertyCollector)
        propcollector.set_attribute_type(pysphere.vi_mor.MORTypes.PropertyCollector)
        request.set_element__this(propcollector)
        request.set_element_token(token)
        return request


class PropertyCollector(PropertiesRequest):
    def __init__(self, vc):
        if vc.get_api_version() >= '4.1':
            self.req = VI.RetrievePropertiesExRequestMsg()
        elif vc.get_api_version() >= '4.0':
            self.req = VI.RetrievePropertiesRequestMsg()

    def call(self, *args, **kwargs):
        return self.RetrieveProperties(*args, **kwargs)

    def populateRequest(self, vc, property_names=[], from_node=None, obj_type='ManagedEntity'):
        request = self.req

        if not from_node:
            from_node = vc._do_service_content.RootFolder

        elif isinstance(from_node, tuple) and len(from_node) == 2:
            from_node = VIMor(from_node[0], from_node[1])
        elif not VIMor.is_mor(from_node):
            raise pysphere.VIException("from_node must be a MOR object or a "
                                       "(<str> mor_id, <str> mor_type) tuple",
                                       pysphere.FaultTypes.PARAMETER_ERROR)

        _this = request.new__this(vc._do_service_content.PropertyCollector)
        _this.set_attribute_type(pysphere.vi_mor.MORTypes.PropertyCollector)

        request.set_element__this(_this)
        do_PropertyFilterSpec_specSet = request.new_specSet()

        props_set = []
        do_PropertySpec_propSet = do_PropertyFilterSpec_specSet.new_propSet()
        do_PropertySpec_propSet.set_element_type(obj_type)
        do_PropertySpec_propSet.set_element_pathSet(property_names)
        props_set.append(do_PropertySpec_propSet)

        objects_set = []
        do_ObjectSpec_objSet = do_PropertyFilterSpec_specSet.new_objectSet()
        mor_obj = do_ObjectSpec_objSet.new_obj(from_node)
        mor_obj.set_attribute_type(from_node.get_attribute_type())
        do_ObjectSpec_objSet.set_element_obj(mor_obj)
        do_ObjectSpec_objSet.set_element_skip(False)

        #Recurse through all ResourcePools
        rp_to_rp = VI.ns0.TraversalSpec_Def('rpToRp').pyclass()
        rp_to_rp.set_element_name('rpToRp')
        rp_to_rp.set_element_type(pysphere.vi_mor.MORTypes.ResourcePool)
        rp_to_rp.set_element_path('resourcePool')
        rp_to_rp.set_element_skip(False)
        rp_to_vm = VI.ns0.TraversalSpec_Def('rpToVm').pyclass()
        rp_to_vm.set_element_name('rpToVm')
        rp_to_vm.set_element_type(pysphere.vi_mor.MORTypes.ResourcePool)
        rp_to_vm.set_element_path('vm')
        rp_to_vm.set_element_skip(False)

        spec_array_resource_pool = [do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet()]
        spec_array_resource_pool[0].set_element_name('rpToRp')
        spec_array_resource_pool[1].set_element_name('rpToVm')

        rp_to_rp.set_element_selectSet(spec_array_resource_pool)

        #Traversal through resource pool branch
        cr_to_rp = VI.ns0.TraversalSpec_Def('crToRp').pyclass()
        cr_to_rp.set_element_name('crToRp')
        cr_to_rp.set_element_type(pysphere.vi_mor.MORTypes.ComputeResource)
        cr_to_rp.set_element_path('resourcePool')
        cr_to_rp.set_element_skip(False)
        spec_array_computer_resource = [do_ObjectSpec_objSet.new_selectSet(),
                                        do_ObjectSpec_objSet.new_selectSet()]
        spec_array_computer_resource[0].set_element_name('rpToRp');
        spec_array_computer_resource[1].set_element_name('rpToVm');
        cr_to_rp.set_element_selectSet(spec_array_computer_resource)

        #Traversal through host branch
        cr_to_h = VI.ns0.TraversalSpec_Def('crToH').pyclass()
        cr_to_h.set_element_name('crToH')
        cr_to_h.set_element_type(pysphere.vi_mor.MORTypes.ComputeResource)
        cr_to_h.set_element_path('host')
        cr_to_h.set_element_skip(False)

        #Traversal through hostFolder branch
        dc_to_hf = VI.ns0.TraversalSpec_Def('dcToHf').pyclass()
        dc_to_hf.set_element_name('dcToHf')
        dc_to_hf.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_hf.set_element_path('hostFolder')
        dc_to_hf.set_element_skip(False)
        spec_array_datacenter_host = [do_ObjectSpec_objSet.new_selectSet()]
        spec_array_datacenter_host[0].set_element_name('visitFolders')
        dc_to_hf.set_element_selectSet(spec_array_datacenter_host)

        #Traversal through vmFolder branch
        dc_to_vmf = VI.ns0.TraversalSpec_Def('dcToVmf').pyclass()
        dc_to_vmf.set_element_name('dcToVmf')
        dc_to_vmf.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_vmf.set_element_path('vmFolder')
        dc_to_vmf.set_element_skip(False)
        spec_array_datacenter_vm = [do_ObjectSpec_objSet.new_selectSet()]
        spec_array_datacenter_vm[0].set_element_name('visitFolders')
        dc_to_vmf.set_element_selectSet(spec_array_datacenter_vm)

        #Traversal through network folder branch
        dc_to_net = VI.ns0.TraversalSpec_Def('dcToNet').pyclass()
        dc_to_net.set_element_name('dcToNet')
        dc_to_net.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_net.set_element_path('networkFolder')
        dc_to_net.set_element_skip(False)
        spec_array_datacenter_net = [do_ObjectSpec_objSet.new_selectSet()]
        spec_array_datacenter_net[0].set_element_name('visitFolders')
        dc_to_net.set_element_selectSet(spec_array_datacenter_net)

        #Traversal through datastore branch
        dc_to_ds = VI.ns0.TraversalSpec_Def('dcToDs').pyclass()
        dc_to_ds.set_element_name('dcToDs')
        dc_to_ds.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_ds.set_element_path('datastore')
        dc_to_ds.set_element_skip(False)
        spec_array_datacenter_ds = [do_ObjectSpec_objSet.new_selectSet()]
        spec_array_datacenter_ds[0].set_element_name('visitFolders')
        dc_to_ds.set_element_selectSet(spec_array_datacenter_ds)

        #Recurse through all hosts
        h_to_vm = VI.ns0.TraversalSpec_Def('hToVm').pyclass()
        h_to_vm.set_element_name('hToVm')
        h_to_vm.set_element_type(pysphere.vi_mor.MORTypes.HostSystem)
        h_to_vm.set_element_path('vm')
        h_to_vm.set_element_skip(False)
        spec_array_host_vm = [do_ObjectSpec_objSet.new_selectSet()]
        spec_array_host_vm[0].set_element_name('visitFolders')
        h_to_vm.set_element_selectSet(spec_array_host_vm)

        #Recurse through the folders
        visit_folders = VI.ns0.TraversalSpec_Def('visitFolders').pyclass()
        visit_folders.set_element_name('visitFolders')
        visit_folders.set_element_type(pysphere.vi_mor.MORTypes.Folder)
        visit_folders.set_element_path('childEntity')
        visit_folders.set_element_skip(False)
        spec_array_visit_folders = [do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet(),
                                    do_ObjectSpec_objSet.new_selectSet()]
        spec_array_visit_folders[0].set_element_name('visitFolders')
        spec_array_visit_folders[1].set_element_name('dcToHf')
        spec_array_visit_folders[2].set_element_name('dcToVmf')
        spec_array_visit_folders[3].set_element_name('dcToNet')
        spec_array_visit_folders[4].set_element_name('crToH')
        spec_array_visit_folders[5].set_element_name('crToRp')
        spec_array_visit_folders[6].set_element_name('dcToDs')
        spec_array_visit_folders[7].set_element_name('hToVm')
        spec_array_visit_folders[8].set_element_name('rpToVm')
        visit_folders.set_element_selectSet(spec_array_visit_folders)

        #Add all of them here
        spec_array = [visit_folders, dc_to_vmf, dc_to_ds, dc_to_hf, dc_to_net, cr_to_h,
                      cr_to_rp, rp_to_rp, h_to_vm, rp_to_vm]

        do_ObjectSpec_objSet.set_element_selectSet(spec_array)
        objects_set.append(do_ObjectSpec_objSet)

        do_PropertyFilterSpec_specSet.set_element_propSet(props_set)
        do_PropertyFilterSpec_specSet.set_element_objectSet(objects_set)
        request.set_element_specSet([do_PropertyFilterSpec_specSet])

        if vc.get_api_version() >= '4.1':
            # set blank options
            options = request.new_options()
            request.set_element_options(options)
            options.set_element_maxObjects(500)


class PropertyEvent(object):
    pass


class VersionEvent(PropertyEvent):
    def __init__(self, newversion):
        self.newversion = newversion


class ObjectSetEvent(PropertyEvent):
    def __init__(self, objectdata):
        self.objectdata = objectdata

        # logging.debug('objectsetevent', extra={'event': objectdata.dictify()})


class PropertyListener(PropertiesRequest):
    def __init__(self, vc):
        if vc.get_api_version() >= '4.1':
            self.req = VI.WaitForUpdatesExRequestMsg()
        else:
            raise Exception('unsupported esx API version: ' + vc.get_api_version())

        self._maxWaitSeconds = 300

    def setMaxWaitSeconds(self, val):
        self._maxWaitSeconds = int(val)

    def getMaxWaitSeconds(self):
        return self._maxWaitSeconds

    def deleteMaxWaitSeconds(self):
        self._maxWaitSeconds = None

    maxWaitSeconds = property(getMaxWaitSeconds, setMaxWaitSeconds, deleteMaxWaitSeconds)

    def call(self, vc, *args, **kwargs):
        return self.WaitForUpdatesEx(vc, *args, **kwargs)

    def createFilter(self, vc, propcol, property_names, motype, from_node):
        request = VI.CreateFilterRequestMsg()

        # the property spec- contains an object set and a property set
        prop_spec = request.new_spec()

        # object set, gets added to the property spec in the end
        obj_set = prop_spec.new_objectSet()

        #   have to translate the from node to a managed object reference
        mor_obj = obj_set.new_obj(from_node)
        mor_obj.set_attribute_type(from_node.get_attribute_type())

        obj_set.set_element_obj(mor_obj)
        obj_set.set_element_skip(False)

        prop_spec.set_element_objectSet([obj_set])

        # big retarded traversal spec
        #Recurse through all ResourcePools
        rp_to_rp = VI.ns0.TraversalSpec_Def('rpToRp').pyclass()
        rp_to_rp.set_element_name('rpToRp')
        rp_to_rp.set_element_type(pysphere.vi_mor.MORTypes.ResourcePool)
        rp_to_rp.set_element_path('resourcePool')
        rp_to_rp.set_element_skip(False)
        rp_to_vm = VI.ns0.TraversalSpec_Def('rpToVm').pyclass()
        rp_to_vm.set_element_name('rpToVm')
        rp_to_vm.set_element_type(pysphere.vi_mor.MORTypes.ResourcePool)
        rp_to_vm.set_element_path('vm')
        rp_to_vm.set_element_skip(False)

        spec_array_resource_pool = [obj_set.new_selectSet(),
                                    obj_set.new_selectSet()]
        spec_array_resource_pool[0].set_element_name('rpToRp')
        spec_array_resource_pool[1].set_element_name('rpToVm')

        rp_to_rp.set_element_selectSet(spec_array_resource_pool)

        #Traversal through resource pool branch
        cr_to_rp = VI.ns0.TraversalSpec_Def('crToRp').pyclass()
        cr_to_rp.set_element_name('crToRp')
        cr_to_rp.set_element_type(pysphere.vi_mor.MORTypes.ComputeResource)
        cr_to_rp.set_element_path('resourcePool')
        cr_to_rp.set_element_skip(False)
        spec_array_computer_resource = [obj_set.new_selectSet(),
                                        obj_set.new_selectSet()]
        spec_array_computer_resource[0].set_element_name('rpToRp');
        spec_array_computer_resource[1].set_element_name('rpToVm');
        cr_to_rp.set_element_selectSet(spec_array_computer_resource)

        #Traversal through host branch
        cr_to_h = VI.ns0.TraversalSpec_Def('crToH').pyclass()
        cr_to_h.set_element_name('crToH')
        cr_to_h.set_element_type(pysphere.vi_mor.MORTypes.ComputeResource)
        cr_to_h.set_element_path('host')
        cr_to_h.set_element_skip(False)

        #Traversal through hostFolder branch
        dc_to_hf = VI.ns0.TraversalSpec_Def('dcToHf').pyclass()
        dc_to_hf.set_element_name('dcToHf')
        dc_to_hf.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_hf.set_element_path('hostFolder')
        dc_to_hf.set_element_skip(False)
        spec_array_datacenter_host = [obj_set.new_selectSet()]
        spec_array_datacenter_host[0].set_element_name('visitFolders')
        dc_to_hf.set_element_selectSet(spec_array_datacenter_host)

        #Traversal through vmFolder branch
        dc_to_vmf = VI.ns0.TraversalSpec_Def('dcToVmf').pyclass()
        dc_to_vmf.set_element_name('dcToVmf')
        dc_to_vmf.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_vmf.set_element_path('vmFolder')
        dc_to_vmf.set_element_skip(False)
        spec_array_datacenter_vm = [obj_set.new_selectSet()]
        spec_array_datacenter_vm[0].set_element_name('visitFolders')
        dc_to_vmf.set_element_selectSet(spec_array_datacenter_vm)

        #Traversal through network folder branch
        dc_to_net = VI.ns0.TraversalSpec_Def('dcToNet').pyclass()
        dc_to_net.set_element_name('dcToNet')
        dc_to_net.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_net.set_element_path('networkFolder')
        dc_to_net.set_element_skip(False)
        spec_array_datacenter_net = [obj_set.new_selectSet()]
        spec_array_datacenter_net[0].set_element_name('visitFolders')
        dc_to_net.set_element_selectSet(spec_array_datacenter_net)

        #Traversal through datastore branch
        dc_to_ds = VI.ns0.TraversalSpec_Def('dcToDs').pyclass()
        dc_to_ds.set_element_name('dcToDs')
        dc_to_ds.set_element_type(pysphere.vi_mor.MORTypes.Datacenter)
        dc_to_ds.set_element_path('datastore')
        dc_to_ds.set_element_skip(False)
        spec_array_datacenter_ds = [obj_set.new_selectSet()]
        spec_array_datacenter_ds[0].set_element_name('visitFolders')
        dc_to_ds.set_element_selectSet(spec_array_datacenter_ds)

        #Recurse through all hosts
        h_to_vm = VI.ns0.TraversalSpec_Def('hToVm').pyclass()
        h_to_vm.set_element_name('hToVm')
        h_to_vm.set_element_type(pysphere.vi_mor.MORTypes.HostSystem)
        h_to_vm.set_element_path('vm')
        h_to_vm.set_element_skip(False)
        spec_array_host_vm = [obj_set.new_selectSet()]
        spec_array_host_vm[0].set_element_name('visitFolders')
        h_to_vm.set_element_selectSet(spec_array_host_vm)

        #Recurse through the folders
        visit_folders = VI.ns0.TraversalSpec_Def('visitFolders').pyclass()
        visit_folders.set_element_name('visitFolders')
        visit_folders.set_element_type(pysphere.vi_mor.MORTypes.Folder)
        visit_folders.set_element_path('childEntity')
        visit_folders.set_element_skip(False)
        spec_array_visit_folders = [obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet(),
                                    obj_set.new_selectSet()]
        spec_array_visit_folders[0].set_element_name('visitFolders')
        spec_array_visit_folders[1].set_element_name('dcToHf')
        spec_array_visit_folders[2].set_element_name('dcToVmf')
        spec_array_visit_folders[3].set_element_name('dcToNet')
        spec_array_visit_folders[4].set_element_name('crToH')
        spec_array_visit_folders[5].set_element_name('crToRp')
        spec_array_visit_folders[6].set_element_name('dcToDs')
        spec_array_visit_folders[7].set_element_name('hToVm')
        spec_array_visit_folders[8].set_element_name('rpToVm')
        visit_folders.set_element_selectSet(spec_array_visit_folders)

        #Add all of them here
        spec_array = [visit_folders, dc_to_vmf, dc_to_ds, dc_to_hf, dc_to_net, cr_to_h,
                      cr_to_rp, rp_to_rp, h_to_vm, rp_to_vm]

        obj_set.set_element_selectSet(spec_array)

        # property set, gets added to the property spec in the end
        prop_set = prop_spec.new_propSet()
        prop_set.set_element_type(motype)
        prop_set.set_element_pathSet(property_names)
        # prop_set.set_element_all(True)        # bleck
        prop_set.set_element_all(False)

        prop_spec.set_element_propSet([prop_set])

        # request options - takes a property collector, the prop spec container, and a boolean
        # for whether or not to permit partial updates
        request.set_element__this(propcol)
        request.set_element_spec(prop_spec)
        request.set_element_partialUpdates(False)

        # raise Exception("STOP")

        with benchmark('createfilter request', logger.debug):
            result = PropertiesRequest._doProxyCall(vc._proxy.CreateFilter, request)

        if result is None:
            raise Exception('CreateFilter request has failed')

        return result._returnval

    @staticmethod
    def _processUpdateSet(updateset):
        for filterset in updateset.FilterSet:
            for objectset in filterset.ObjectSet:
                logger.debug('objectset for %s', objectset._obj)
                yield objectset

    @property
    def version(self):
        if self.req is None:
            return ''
        else:
            return self.req.Version

    def WaitForUpdatesEx(self, vc, maxObjectUpdates=None, **kwargs):
        # import ipdb
        # ipdb.set_trace()

        assert (vc.get_api_version() >= '4.1')

        options = self.req._options

        options.set_element_maxObjectUpdates(maxObjectUpdates)
        options.set_element_maxWaitSeconds(self.maxWaitSeconds)

        with benchmark('first update received', logger.debug):
            result = PropertiesRequest._doProxyCall(vc._proxy.WaitForUpdatesEx, self.req)

        ret = result._returnval

        while ret is not None:
            logger.debug('current version \'%s\'', ret.Version)

            self.req.set_element_version(ret.Version)

            yield VersionEvent(self.version)

            with benchmark('finished processing version \'%s\'' % ret.Version, logger.debug):
                for result in self._processUpdateSet(ret):
                    yield ObjectSetEvent(result)

            options.set_element_maxWaitSeconds(self.maxWaitSeconds)
            with benchmark('fetched version \'%s\', wait time' % ret.Version, logger.debug):
                result = PropertiesRequest._doProxyCall(vc._proxy.WaitForUpdatesEx, self.req)

            ret = result._returnval

    def populateRequest(self, vc, property_names=[], from_node=None, obj_type='ManagedEntity'):
        # process the from_node argument
        if not from_node:
            from_node = vc._do_service_content.RootFolder

        elif isinstance(from_node, tuple) and len(from_node) == 2:
            from_node = VIMor(from_node[0], from_node[1])
        elif not VIMor.is_mor(from_node):
            raise pysphere.VIException("from_node must be a MOR object or a (<str> mor_id, <str> mor_type) tuple",
                                       pysphere.FaultTypes.PARAMETER_ERROR)

        request = self.req

        _this = request.new__this(vc._do_service_content.PropertyCollector)
        _this.set_attribute_type(pysphere.vi_mor.MORTypes.PropertyCollector)

        request.set_element__this(_this)

        # create a filter
        _filter = self.createFilter(vc, _this, property_names, obj_type, from_node)

        # set blank options
        options = request.new_options()
        request.set_element_options(options)

        self.resetRequestVersion()

    def resetRequestVersion(self):
        request = self.req
        request.set_element_version("")


# sample usage
if __name__ == '__main__':
    VCENTER_SERVER = "vcenter"
    VCENTER_USER = "root"
    VCENTER_PASS = "vmware"

    with benchmark("everything"):
        with benchmark("vi connection"):
            vc = pysphere.VIServer()
            vc.connect(VCENTER_SERVER, VCENTER_USER, VCENTER_PASS)

        p = PropertyListener(vc)
        p.maxWaitSeconds = 300
        # p = PropertyCollector()
        # p.populateRequest(vc, ['name', 'config.uuid', 'config.hardware.numCPU', 'config.hardware.memoryMB'], None, 'VirtualMachine')
        props = ["summary.config.uuid", "summary.config.name", "runtime.host", "guest.hostName", "guest.guestFullName",
                 "config.guestFullName", "config.alternateGuestName", "summary.config.memorySizeMB",
                 "summary.config.numCpu", "summary.config.annotation", "guest.net", "config.hardware.device",
                 "layoutEx.file", "summary.customValue", "runtime.powerState", "summary.guest.toolsStatus",
                 "guestHeartbeatStatus", "availableField", "summary.config.template", "summary.storage"]
        p.populateRequest(vc, props, None, 'VirtualMachine')

        with benchmark('propcollector-request'):
            it = p.call(vc)

        with benchmark('enumerate'):
            count = 0
            for ix in it:
                for jx in ix.ChangeSet:
                    print dir(jx)
                    try:
                        print jx.Name, jx.Op, jx.Val
                    except Exception, e:
                        jx.Name

# vim: set ts=4 sw=4 expandtab:

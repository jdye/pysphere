from pysphere.resources import VimService_services as VI
from pysphere.vi_property import VIProperty
from pysphere.resources.vi_exception import VIException, VIApiException, \
    UnsupportedPerfIntervalError, FaultTypes
from pysphere.vi_mor import VIMor
from pysphere.vi_task import VITask
from pysphere.resources import VimService_services, VimService_services_types


class FileManager:
    def __init__(self, server, mor):
        """
        :type server: VIServer
        :type mor: VIMor
        """
        self._server = server
        self._mor = mor
        self._properties = VIProperty(server, mor)

    def MakeDirectory(self, name, datacenter=None, createParentDirectories=None):
        """
        :type name: VIServer
        :type datacenter: VIMor
        :type createParentDirectories: bool
        """
        request = VI.MakeDirectoryRequestMsg()
        _this = request.new__this(self._mor)
        request.set_element__this(_this)

        if createParentDirectories is not None:
            request.CreateParentDirectories = createParentDirectories

        if not VIMor.is_mor(datacenter):
            datacenter = VIMor(datacenter, 'Datacenter')

        request.Datacenter = datacenter
        request.Name = name

        self._server._proxy.MakeDirectory(request)

    def MoveDatastoreFile(self, sourceName, sourceDatacenter, destinationName, destinationDatacenter, force=False,
                          sync_run=True):
        """
        :type sourceName: basestring
        :type sourceDatacenter: VIMor
        :type destinationName: basestring
        :type destinationDatacenter: VIMor
        :type force: bool
        :type sync_run: bool
        :return: VITask
        :rtype: VITask
        :raises: VIApiException
        """
        if not VIMor.is_mor(sourceDatacenter):
            sourceDatacenter = VIMor(sourceDatacenter, 'Datacenter')

        if not VIMor.is_mor(destinationDatacenter):
            destinationDatacenter = VIMor(destinationDatacenter, 'Datacenter')

        request = VI.MoveDatastoreFile_TaskRequestMsg()
        _this = request.new__this(self._mor)
        request.set_element__this(_this)

        request.SourceDatacenter = sourceDatacenter
        request.DestinationDatacenter = destinationDatacenter
        request.SourceName = sourceName
        request.DestinationName = destinationName
        request.Force = force

        return self._generic_task(request, self._server._proxy.MoveDatastoreFile_Task, sync_run)

    def ChangeOwner(self, name, datacenter, owner):
        """
        :type name: VIServer
        :type datacenter: VIMor
        :type owner: basestring
        """
        # NOTE vsphere always returns not supported.  api docs says method is not supported.  what?
        request = VI.ChangeOwnerRequestMsg()
        _this = request.new__this(self._mor)
        request.set_element__this(_this)

        if not VIMor.is_mor(datacenter):
            datacenter = VIMor(datacenter, 'Datacenter')

        request.Datacenter = datacenter
        request.Name = name
        request.Owner = owner

        self._server._proxy.ChangeOwner(request)

    def CopyDatastoreFile(self, sourceName, sourceDatacenter, destinationName, destinationDatacenter, force=False,
                          sync_run=True):
        """
        :type sourceName: basestring
        :type sourceDatacenter: VIMor
        :type destinationName: basestring
        :type destinationDatacenter: VIMor
        :type force: bool
        :type sync_run: bool
        :return: VITask
        :rtype: VITask
        :raises: VIApiException
        """
        if not VIMor.is_mor(sourceDatacenter):
            sourceDatacenter = VIMor(sourceDatacenter, 'Datacenter')

        if not VIMor.is_mor(destinationDatacenter):
            destinationDatacenter = VIMor(destinationDatacenter, 'Datacenter')

        request = VI.CopyDatastoreFile_TaskRequestMsg()
        _this = request.new__this(self._mor)
        request.set_element__this(_this)

        request.SourceDatacenter = sourceDatacenter
        request.DestinationDatacenter = destinationDatacenter
        request.SourceName = sourceName
        request.DestinationName = destinationName
        request.Force = force

        return self._generic_task(request, self._server._proxy.CopyDatastoreFile_Task, sync_run)

    def DeleteDatastoreFile(self, name, datacenter, sync_run=True):
        """
        :type name: basestring
        :type datacenter: VIMor
        :type sync_run: boool
        :return: VITask
        :rtype: VITask
        :raises: VIApiException
        """
        if not VIMor.is_mor(datacenter):
            datacenter = VIMor(datacenter, 'Datacenter')

        request = VI.DeleteDatastoreFile_TaskRequestMsg()
        _this = request.new__this(self._mor)
        request.set_element__this(_this)

        request.Datacenter = datacenter
        request.Name = name

        return self._generic_task(request, self._server._proxy.DeleteDatastoreFile_Task, sync_run)

    ###################
    # Private methods #
    ###################

    def _generic_task(self, request, taskfunc, sync_run=True):
        task = taskfunc(request)._returnval
        vi_task = VITask(task, self._server)

        try:
            if sync_run:
                status = vi_task.wait_for_state([vi_task.STATE_SUCCESS, vi_task.STATE_ERROR])
                if status == vi_task.STATE_ERROR:
                    raise VIException(vi_task.get_error_message(), FaultTypes.TASK_ERROR)
            return vi_task
        except (VI.ZSI.FaultException), e:
            raise VIApiException(e)
